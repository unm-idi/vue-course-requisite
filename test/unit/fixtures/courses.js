export default [
  {
    id: 1,
    full_number: 'ECE 101'
  },
  {
    id: 2,
    full_number: 'ECE 102',
    coreq: {
      type: 'course', course: { id: 1 }
    }
  },
  {
    id: 3,
    full_number: 'ECE 201',
    prereq: {
      type: 'or',
      operands: [
        { type: 'course', course: { id: 1 } },
        { type: 'course', course: { id: 2 } }
      ]
    }
  },
  {
    id: 4,
    full_number: 'ECE 202',
    prereq: {
      type: 'or',
      operands: [
        { type: 'course', course: { id: 1 } },
        { type: 'course', concurrency_ind: true, course: { id: 2 } }
      ]
    },
    coreq: {
      type: 'course', course: { id: 3 }
    }
  },
  {
    id: 5,
    full_number: 'ECE 203',
    prereq: {
      type: 'and',
      operands: [
        { type: 'course', course: { id: 1 } },
        { type: 'course', course: { id: 2 } }
      ]
    }
  },
  {
    id: 6,
    full_number: 'ECE 301',
    prereq: {
      type: 'or',
      operands: [
        {
          type: 'and',
          operands: [
            { type: 'course', course: { id: 1 } },
            { type: 'course', course: { id: 2 } }
          ]
        },
        { type: 'course', course: { id: 3 } }
      ]
    }
  },
  {
    id: 7,
    full_number: 'MATH 101'
  },
  {
    id: 8,
    full_number: 'MATH 102'
  },
  {
    id: 9,
    full_number: 'MATH 251'
  },
  {
    id: 10,
    full_number: 'MATH 252'
  },
  {
    id: 11,
    full_number: 'MATH 263'
  },
  {
    id: 12,
    full_number: 'MATH 350'
  },
  {
    id: 13,
    full_number: 'MATH 370'
  },
  {
    id: 14,
    full_number: 'MATH 470'
  }
]
