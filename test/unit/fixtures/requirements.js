export default [
  {
    'type': 'CourseRequirement',
    'course': {
      'id': 1
    }
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': false,
        'min_hours': '',
        'max_hours': '',
        'code_string': 'MATH',
        'match_string': '101',
        'minimum_match_number': '',
        'maximum_match_number': ''
      }
    ]
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': false,
        'min_hours': '',
        'max_hours': '',
        'code_string': '*',
        'match_string': '',
        'minimum_match_number': '300',
        'maximum_match_number': '399'
      }
    ]
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': false,
        'min_hours': '',
        'max_hours': '',
        'code_string': 'MATH',
        'match_string': '*',
        'minimum_match_number': '',
        'maximum_match_number': ''
      }
    ]
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': false,
        'min_hours': '',
        'max_hours': '',
        'code_string': 'MATH',
        'match_string': '',
        'minimum_match_number': '100',
        'maximum_match_number': '199'
      }
    ]
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': true,
        'min_hours': '',
        'max_hours': '',
        'code_string': 'MATH',
        'match_string': '',
        'minimum_match_number': '100',
        'maximum_match_number': '199'
      }
    ]
  },
  {
    'type': 'WildCardRequirement',
    'rules': [
      {
        'exclude': true,
        'min_hours': '',
        'max_hours': '',
        'code_string': 'MATH',
        'match_string': '',
        'minimum_match_number': '100',
        'maximum_match_number': '199'
      },
      {
        'exclude': false,
        'min_hours': '',
        'max_hours': '',
        'code_string': '*',
        'match_string': '101',
        'minimum_match_number': '',
        'maximum_match_number': ''
      }
    ]
  },
  {
    'type': 'DegreeRequirement',
    'subrequirements': [
      {
        'type': 'CourseRequirement',
        'course': {
          'id': 4
        }
      },
      {
        'type': 'WildCardRequirement',
        'rules': [
          {
            'exclude': false,
            'min_hours': '',
            'max_hours': '',
            'code_string': 'MATH',
            'match_string': '*',
            'minimum_match_number': '',
            'maximum_match_number': ''
          }
        ]
      }
    ]
  },
  {
    'type': 'CoreRequirement',
    'core': {
      'type': 'DegreeRequirement',
      'subrequirements': [
        {
          'type': 'WildCardRequirement',
          'rules': [
            {
              'exclude': false,
              'min_hours': '',
              'max_hours': '',
              'code_string': '*',
              'match_string': '',
              'minimum_match_number': '300',
              'maximum_match_number': '399'
            }
          ]
        },
        {
          'type': 'DegreeRequirement',
          'subrequirements': [
            {
              'type': 'WildCardRequirement',
              'rules': [
                {
                  'exclude': false,
                  'min_hours': '',
                  'max_hours': '',
                  'code_string': 'MATH',
                  'match_string': '*',
                  'minimum_match_number': '',
                  'maximum_match_number': ''
                },
                {
                  'exclude': true,
                  'min_hours': '',
                  'max_hours': '',
                  'code_string': 'MATH',
                  'match_string': '',
                  'minimum_match_number': '100',
                  'maximum_match_number': '199'
                }
              ]
            },
            {
              'type': 'CourseRequirement',
              'course': {
                'id': 3
              }
            }
          ]
        }
      ]
    }
  }
]
