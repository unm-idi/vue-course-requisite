import requisiteValidator from '@/helpers/requisiteValidator'
import courses from '../fixtures/courses.js'

describe('requisiteValidator', () => {
  describe('prereq validation', () => {
    it('should give warning and missing courses list for missing prereq', () => {
      const course = courses[2]
      const terms = [[course]]
      const report = requisiteValidator(course, 0, terms)

      expect(report.prereq.code).to.equal('warning')
      expect(report.prereq.missingCourses.length).to.equal(2)
    })

    it('should validate successfully with "or" operand', () => {
      const course = courses[2]
      const terms = [[courses[0]], [course]]
      const report = requisiteValidator(course, 1, terms)

      expect(report.prereq.code).to.equal('valid')
    })

    it('should validate successfully with "and" operand', () => {
      const course = courses[4]
      const terms = [[courses[0], courses[1]], [course]]
      const report = requisiteValidator(course, 1, terms)

      expect(report.prereq.code).to.equal('valid')
    })

    it('should give error and offending courses if invalid', () => {
      const course = courses[4]
      const terms = [[courses[0]], [course], [courses[1]]]
      const report = requisiteValidator(course, 1, terms)

      expect(report.prereq.code).to.equal('error')
      expect(report.prereq.offendingCourses.length).to.equal(1)
    })

    it('should be valid even with offending course', () => {
      const course = courses[2]
      const terms = [[courses[0]], [course], [courses[1]]]
      const report = requisiteValidator(course, 1, terms)

      expect(report.prereq.code).to.equal('valid')
    })

    it('should be valid with concurrency indicator', () => {
      const course = courses[3]
      const terms = [[course, courses[1]]]
      const report = requisiteValidator(course, 0, terms)

      expect(report.prereq.code).to.equal('valid')
    })
  })

  describe('coreq validation', () => {
    it('should give warning for missing coreq', () => {
      const course = courses[1]
      const terms = [[course]]
      const report = requisiteValidator(course, 0, terms)

      expect(report.coreq.code).to.equal('warning')
    })

    it('should validate successfully', () => {
      const course = courses[1]
      const terms = [[courses[0], course]]
      const report = requisiteValidator(course, 0, terms)

      expect(report.coreq.code).to.equal('valid')
    })

    it('should give error and offending courses', () => {
      const course = courses[1]
      const terms = [[courses[0]], [course]]
      const report = requisiteValidator(course, 1, terms)

      expect(report.coreq.code).to.equal('error')
      expect(report.coreq.offendingCourses.length).to.equal(1)
    })
  })
})
