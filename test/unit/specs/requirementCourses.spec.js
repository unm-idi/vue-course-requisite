import requirementCourses from '@/helpers/requirementCourses'
import { default as allCourses } from '../fixtures/courses.js'
import requirements from '../fixtures/requirements.js'

describe('requirementCourses', () => {
  it('should return a single matched course for a simple course requirement', () => {
    const courses = requirementCourses(requirements[0], allCourses)
    expect(courses.length).to.equal(1)
  })

  it('should return a single matched course for a wildcard requirement with a MATH code string and 101 match string', () => {
    const courses = requirementCourses(requirements[1], allCourses)
    expect(courses.length).to.equal(1)
  })

  it('should return matched courses for a wildcard requirement with a * code string and number range', () => {
    const courses = requirementCourses(requirements[2], allCourses)
    expect(courses.length).to.equal(3)
  })

  it('should return matched courses for a wildcard requirement with a MATH code string and * match string', () => {
    const courses = requirementCourses(requirements[3], allCourses)
    expect(courses.length).to.equal(8)
  })

  it('should return matched courses for a wildcard requirement with a MATH code string and number range', () => {
    const courses = requirementCourses(requirements[4], allCourses)
    expect(courses.length).to.equal(2)
  })

  it('should return matched courses for a wildcard requirement with an exculsion MATH code string and * match string', () => {
    const courses = requirementCourses(requirements[5], allCourses)
    expect(courses.length).to.equal(12)
  })

  it('should return matched courses for a wildcard requirement with multiple rules', () => {
    const courses = requirementCourses(requirements[6], allCourses)
    expect(courses.length).to.equal(1)
  })

  it('should return matched courses for a simple nested requirement structure', () => {
    const courses = requirementCourses(requirements[7], allCourses)
    expect(courses.length).to.equal(9)
  })

  it('should return matched courses for a complex nested requirement structure', () => {
    const courses = requirementCourses(requirements[8], allCourses)
    expect(courses.length).to.equal(8)
  })
})
