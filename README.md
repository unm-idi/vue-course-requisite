# vue-course-requisite

> Vue plugin for displaying, configuring, and modifying course pre and co requisites.

See a working [demo](https://unm-idi.gitlab.io/vue-course-requisite/).

## Build Setup

``` bash
# install dependencies
yarn install

or

npm install

# serve demo with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build demo (used to prepare for gitlab pages)
npm run build:demo

# run unit tests
npm run unit

# run all tests
npm test
```
