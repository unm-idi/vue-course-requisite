// This file will build the demo
import 'babel-polyfill'

import Vue from 'vue'
import Demo from './Demo'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<Demo/>',
  components: {
    Demo
  }
})
