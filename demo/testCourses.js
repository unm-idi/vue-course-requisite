export default [
  {
    id: 1,
    full_number: 'ECE 101',
    full_name: 'ECE 101 - Engineering I',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    prereq: {
      'type': 'and',
      'operands': [
        {
          'type': 'course',
          'course': {
            'id': 1,
            'full_number': 'ECE 101',
            'full_name': 'ECE 101 - Engineering I',
            'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          },
          'minimum_course_grade': 'C',
          'concurrency_ind': false
        },
        {
          'type': 'or',
          'operands': [
            {
              'type': 'placement_test',
              'placement_test': {
                'id': 1,
                'name': 'ACT'
              },
              'minimum_test_score': 320
            },
            {
              'type': 'course',
              'course': {
                'id': 2,
                'full_number': 'ECE 102',
                'full_name': 'ECE 102 - Engineering II',
                'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
              },
              'minimum_course_grade': 'C',
              'concurrency_ind': false
            }
          ]
        }
      ]
    }
  },
  {
    id: 2,
    full_number: 'ECE 102',
    full_name: 'ECE 102 - Engineering II',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    prereq: {
      'type': 'and',
      'operands': [
        {
          'type': 'course',
          'course': {
            'id': 1,
            'full_number': 'ECE 101',
            'full_name': 'ECE 101 - Engineering I',
            'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          },
          'minimum_course_grade': 'C',
          'concurrency_ind': false
        },
        {
          'type': 'or',
          'operands': [
            {
              'type': 'placement_test',
              'placement_test': {
                'id': 1,
                'name': 'ACT'
              },
              'minimum_test_score': 320
            },
            {
              'type': 'course',
              'course': {
                'id': 2,
                'full_number': 'ECE 102',
                'full_name': 'ECE 102 - Engineering II',
                'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
              },
              'minimum_course_grade': 'C',
              'concurrency_ind': false
            }
          ]
        }
      ]
    },
    coreq: {
      'type': 'course',
      'course': {
        'id': 1,
        'full_number': 'ECE 101',
        'full_name': 'ECE 101 - Engineering I',
        'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
      }
    }
  }
]
