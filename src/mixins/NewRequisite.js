export default {
  methods: {
    newRequisite (type) {
      if (type === 'course') {
        return {
          type: 'course',
          course: this.courses[0] || this.allCourses[0],
          minimum_course_grade: 'C',
          concurrency_ind: false
        }
      } else if (type === 'placement_test') {
        return {
          type: 'placement_test',
          placement_test: this.placementTests[0],
          minimum_test_score: 320
        }
      } else if (type === 'custom') {
        return {
          type: 'custom',
          description: ''
        }
      }
    }
  }
}
