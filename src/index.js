// This file will build the plugin
import RequisitesInterface from './components/RequisitesInterface.vue'
import RequisitesString from './components/RequisitesString.vue'
import requisiteValidator from './helpers/requisiteValidator.js'
import requisiteStringify from './helpers/requisiteStringify.js'
import requirementCourses from './helpers/requirementCourses.js'

import './styles/vue-course-requisite.scss'

export default RequisitesInterface

export { RequisitesInterface, RequisitesString, requisiteValidator, requisiteStringify, requirementCourses }
