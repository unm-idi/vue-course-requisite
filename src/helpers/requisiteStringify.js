const formString = function (operand, caps = true, paren = false) {
  if (!operand) {
    return null
  } else if (operand.operands) {
    const str = operand.operands
      .map(o => formString(o, caps, true))
      .filter(s => s)
      .join(` ${caps ? operand.type.toUpperCase() : operand.type.toLowerCase()} `)
    return paren ? `( ${str} )` : str
  } else {
    switch (operand.type) {
      case 'course':
        return operand.course.full_number
      case 'placement_test':
        return operand.placement_test.name
      case 'custom':
        return operand.description
    }
  }
}

export default formString
