import { uniqBy, includes, some, filter, reject } from 'lodash'

// The exposed API is used to decide which courses from an array are part of a
// requirement and it's tree

// @param {Object} requirement: top level requirement refer to test/unit/fixtures/requirements.js
// for examples of how the requirements should be formed:

// @param {Array} allCourses: should be in the following form:
// {
//   id: 1,
//   full_number: 'ECE 101'
// }

const scanRequirement = function (requirement, allCourses) {
  return uniqBy(scanRequirement[requirement.type](requirement, allCourses), 'id')
}

// CourseRequirement scanner
scanRequirement.CourseRequirement = function (requirement) {
  return [requirement.course]
}

// DegreeRequirement scanner
scanRequirement.DegreeRequirement = function (requirement, allCourses) {
  let courses = []

  requirement.subrequirements.forEach(r => {
    courses = courses.concat(scanRequirement(r, allCourses))
  })

  return courses
}

// CoreRequirement scanner
scanRequirement.CoreRequirement = function (requirement, allCourses) {
  return scanRequirement(requirement.core, allCourses)
}

// WildCardRequirement scanner
scanRequirement.WildCardRequirement = function (requirement, allCourses) {
  let courses = allCourses.map(c => {
    return {
      // code: 'ECE 101'
      code: c.full_number,
      // extract consecutive number sets from the full number
      // numbers: [101]
      numbers: c.full_number.match(/\d+/g).map(Number),
      course: c
    }
  })

  requirement.rules.forEach(rule => {
    courses = (rule.exclude ? reject : filter)(courses, c => {
      if (!rule.code_string && !rule.match_string && !rule.maximum_match_number && !rule.minimum_match_number) return !rule.exclude

      return (!rule.code_string || rule.code_string === '*' || c.code.match(new RegExp(`^${rule.code_string}([^a-zA-Z]|$)`, 'ig'))) &&
             (!rule.match_string || rule.match_string === '*' || includes(c.numbers, +rule.match_string)) &&
             (!rule.minimum_match_number || some(c.numbers, n => n >= +rule.minimum_match_number)) &&
             (!rule.maximum_match_number || some(c.numbers, n => n <= +rule.maximum_match_number))
    })
  })

  return courses.map(c => c.course)
}

export default scanRequirement
